using Core.DependencyResolvers;
using Core.Extensions;
using Core.Utilities.IoC;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebUI.Tasks.Triggers;

namespace WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.LoginPath = "/giris";
                        options.LogoutPath = "/cikis";
                        options.AccessDeniedPath = "/auths/AccessDenied";
                        options.ExpireTimeSpan = System.TimeSpan.FromHours(10);
                    }); //Eklendi

            services.AddDependencyResolvers(new ICoreModule[]
              {
                    new CoreModule()
              }); //eklendi 
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.ConfigureCustomExeptionMiddleware(); //eklendi

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication(); //eklendi

            app.UseAuthorization();

            GetDovizTrigger.Run();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "anasayfa",
                    pattern: "anasayfa",
                    defaults: new { controller = "Home", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "giris",
                    pattern: "giris",
                    defaults: new { controller = "Auths", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "cikis",
                    pattern: "cikis",
                    defaults: new { controller = "Auths", action = "Logout" });

                endpoints.MapControllerRoute(
                    name: "urunler",
                    pattern: "urunler",
                    defaults: new { controller = "Products", action = "GetProduct" });

                endpoints.MapControllerRoute(
                    name: "urun-ekle",
                    pattern: "urun-ekle",
                    defaults: new { controller = "Products", action = "Add" });

                endpoints.MapControllerRoute(
                    name: "kategoriler",
                    pattern: "kategoriler",
                    defaults: new { controller = "Categories", action = "GetAll" });

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
