﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Views.Home.Lang
{
    public static class IndexLangExtension
    {
        public static string tr(this string value)
        {
            var culture = CultureInfo.CurrentCulture.Name;
            
            if (culture == "en-GB")
            {
                return value switch
                {
                    "home" => "Home",
                    _ => throw new Exception("not found")
                };
            }
            else if (culture == "en-US")
            {
                return value switch
                {
                    "home" => "Home",
                    _ => throw new Exception("not found")
                };
            }
            else
            {
                return value switch
                {
                    "home" => "anasayfa",
                    _ => throw new Exception("bulunamadı")
                };   
            }
        }
    }
}
