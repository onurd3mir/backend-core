﻿using Business.Abstract;
using Core.Entities.Concrete;
using Entities.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Core.Extensions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace WebUI.Controllers
{
    public class AuthsController : Controller
    {
        private IAuthService _authService;
        private IUserService _userService; 
        public AuthsController(IAuthService authService,IUserService userService)
        {
            _authService = authService;
            _userService = userService;
        }

        public IActionResult Index(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            var userToCheck = _authService.Login(userForLoginDto);
            if(!userToCheck.Success)
            {
                ModelState.AddModelError("", userToCheck.Message);

                return View("/Views/Auth/Index.cshtml");
            }

            var claimRoles = _userService.GetCliams(userToCheck.Data);
            var claims = SetClaims(userToCheck.Data,claimRoles);
            
            var claimsIdentity = new ClaimsIdentity(claims, "Cookies", "user", "role");
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            await HttpContext.SignInAsync
                (CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal, new AuthenticationProperties
            {
                IsPersistent = false,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(10),

            });

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect("/");
            }
        }

        private IEnumerable<Claim> SetClaims(User user, List<OperationClaim> operationClaims)
        {
            var claims = new List<Claim>();
            claims.AddEmail(user.Email);
            claims.AddNameIdentifier(user.Id.ToString());
            claims.AddName($"{user.FirstName} {user.LastName}");
            claims.AddRoles(operationClaims.Select(c => c.Name).ToArray());
            return claims;
        }

        public IActionResult AccessDenied(string returnUrl = null)
        {
            if (!User.Identity.IsAuthenticated) return Redirect("/giris");

            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Redirect("giris");
        }

    }
}
