﻿using Business.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class CategoriesController : Controller
    {
        private IMyAggregateService _myAggregateService;
        public CategoriesController(IMyAggregateService myAggregateService)
        {
            _myAggregateService = myAggregateService;
        }

        public IActionResult Index()
        {
            return View();
        }
 
        public IActionResult GetAll()
        {
            return View(_myAggregateService.CategoryService.GetList().Data);
        }

    }
}
