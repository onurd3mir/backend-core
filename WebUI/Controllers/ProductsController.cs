﻿using Business.Abstract;
using Entities.Concrete;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private IProductService _productService;
        private ICategoryService _categoryService;
       
        public ProductsController(IProductService productService,ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }
        
        public IActionResult GetProduct()
        {
            var result = _productService.GetList();
            return View(result.Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            var model = new ProductAddModel
            {
                Product = new Product(),
                Categories = _categoryService.GetList().Data
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Add(Product product)
        { 
            try
            {
                var result = _productService.Add(product);
                return RedirectToAction("getproduct");
            }
            catch (ValidationException e)
            {
                IEnumerable<ValidationFailure> errors = ((ValidationException)e).Errors;
                errors.ToList().ForEach(f => {
                    ModelState.AddModelError("Product." + f.PropertyName, f.ErrorMessage);
                });
                var model = new ProductAddModel
                {
                    Product = product,
                    Categories = _categoryService.GetList().Data
                };
                return View(model);
            }
        }
    }
}
