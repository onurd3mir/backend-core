﻿using Quartz;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebUI.Tasks.Jobs
{
    public class GetDovizJob : IJob
    {
        private HttpClient _httpClient;
        private string url = "https://www.tcmb.gov.tr/kurlar/today.xml";

        public GetDovizJob()
        {
            _httpClient = new HttpClient();
        }

        public Task Execute(IJobExecutionContext context)
        {
            GetDoviz();
            return Task.CompletedTask;
        }

        void GetDoviz()
        {
            string doviz = _httpClient.GetStringAsync(url).Result.ToString();
            Console.WriteLine(doviz);
        }
    }
}
