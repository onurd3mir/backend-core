﻿using Quartz;
using Quartz.Impl;
using WebUI.Tasks.Jobs;

namespace WebUI.Tasks.Triggers
{
    public class GetDovizTrigger
    {
        public static async void Run()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();

            if (!scheduler.IsStarted)
                await scheduler.Start();

            var jobDetail = JobBuilder.Create<GetDovizJob>().Build();

            var cronTrigger = TriggerBuilder.Create()
                .WithIdentity("GetDovizJob")
                //.StartNow()
                //.WithSimpleSchedule(builder => builder.WithIntervalInMinutes(1).RepeatForever())
                .WithCronSchedule("0 0/1 * * * ?")
                .Build();

            await scheduler.ScheduleJob(jobDetail, cronTrigger);
        }
    }
}
