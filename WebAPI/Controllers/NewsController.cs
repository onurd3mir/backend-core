﻿using Business.Abstract;
using Entities.Mongo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpPost("add")]
        public IActionResult Add([FromBody] News news)
        {
            return Ok(_newsService.Add(news));
        }

        [HttpGet("getlist")]
        public IActionResult GetList()
        {
            return Ok(_newsService.GetList());
        }

        [HttpGet("getbyid")]
        public IActionResult GetById(string id)
        {
            return Ok(_newsService.Get(id));
        }

        [HttpGet("delete")]
        public IActionResult Delete(string id)
        {
            return Ok(_newsService.Delete(id));
        }

        [HttpPost("update")]
        public IActionResult Update(string id,[FromBody] News news)
        {
            return Ok(_newsService.Update(id,news));
        }

    }
}
