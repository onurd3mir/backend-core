﻿using Core.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Core.DataAccess.Mongo
{
    public class MongoEntityRepositoryBase<TEntity> : IMongoEntityRepository<TEntity>
        where TEntity : DocumentDbEntity, new()
    {      
        private readonly IMongoCollection<TEntity> _collection;

        public MongoEntityRepositoryBase()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var db = client.GetDatabase("Test");
            _collection = db.GetCollection<TEntity>(typeof(TEntity).Name.ToLowerInvariant());
        }

        public TEntity Add(TEntity entity)
        {
            _collection.InsertOne(entity);
            return entity;
        }

        public void Delete(string id)
        {
            _collection.DeleteOne(n => n.Id == id);
        }

        public TEntity Get(Expression<Func<TEntity, bool>> filter)
        {
            return _collection.Find(filter).FirstOrDefault();
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter != null
                ? _collection.Find(filter).ToList()
                : _collection.Find(news => true).ToList();
        }

        public void Update(string id, TEntity entity)
        {
            _collection.ReplaceOne(n => n.Id == id, entity);
        }
    }
}
