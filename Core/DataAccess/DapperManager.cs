﻿using Dapper;
using System;
using System.Data;
using System.Linq;

namespace Core.DataAccess
{
    public class DapperManager
    {
        private IDbConnection _dbConnection;

        public DapperManager(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public int ExecuteQuery(string sql)
        {
            try
            {
                _dbConnection.Open();
                return _dbConnection.Execute(sql);
            }
            catch (Exception err)
            {
                throw new NotImplementedException(err.Message);
            }
            finally { _dbConnection.Close(); }
        }

        public IQueryable<T> GetQuery<T>(string sql)
        {
            try
            {
                _dbConnection.Open();
                return _dbConnection.Query<T>(sql).AsQueryable();
            }
            catch (Exception err)
            {
                throw new NotImplementedException(err.Message);
            }
            finally { _dbConnection.Close(); }
 
        }
    }
}
