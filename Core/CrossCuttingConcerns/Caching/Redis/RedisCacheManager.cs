﻿using Core.Utilities.Results;
using StackExchange.Redis;
using System;

namespace Core.CrossCuttingConcerns.Caching.Redis
{
    public class RedisCacheManager : JsonResult, IRedisCacheManager
    {
        private IDatabase _db;
        private readonly string host = "192.168.133.130";
        private readonly int port = 6379;

        public RedisCacheManager()
        {
            CreateRedisDB();
        }

        private IDatabase CreateRedisDB()
        {
            if (null == _db)
            {
                ConfigurationOptions option = new ConfigurationOptions();
                option.Ssl = false;
                option.EndPoints.Add(host, port);
                var connect = ConnectionMultiplexer.Connect(option);
                _db = connect.GetDatabase();
            }

            return _db;
        }

        public void Add(string key, object data, int duration)
        {
            string entryBytes = SerializeString(data);
            _db.StringSet(key, entryBytes);

            var expiresIn = TimeSpan.FromMinutes(duration);

            if (duration > 0)
                _db.KeyExpire(key, expiresIn);
        }

        public T Get<T>(string key)
        {
            var rValue = _db.StringGet(key);
            var result = DeserializeString<T>(rValue);
            return result;
        }

        public object Get(string key)
        {
            string rValue = _db.StringGet(key);
            object rData = DeserializeString(rValue);
            return rData;
        }

        public bool IsAdd(string key)
        {
            return _db.KeyExists(key);
        }

        public void Remove(string key)
        {
            _db.KeyDelete(key);
        }

        public void RemoveByPattern(string pattern)
        {
            var server = _db.Multiplexer.GetServer(host, port);
            foreach (var item in server.Keys(pattern: "*" + pattern + "*"))
                _db.KeyDelete(item);
        }

        public void Clear()
        {
            var server = _db.Multiplexer.GetServer(host, port);
            foreach (var item in server.Keys())
                _db.KeyDelete(item);
        }

        #region SRC

        // https://github.com/ozaksuty/Redis

        //public T Get<T>(string key)
        //{
        //    var rValue = _db.SetMembers(key);
        //    if (rValue.Length == 0)
        //        return default(T);

        //    var result = Deserialize<T>(rValue.ToStringArray());
        //    return result;
        //}

        //public void Set(string key, object data, int cacheTime)
        //{
        //    if (data == null)
        //        return;

        //    var entryBytes = Serialize(data);
        //    _db.SetAdd(key, entryBytes);

        //    var expiresIn = TimeSpan.FromMinutes(cacheTime);

        //    if (cacheTime > 0)
        //        _db.KeyExpire(key, expiresIn);
        //}

        #endregion
    }
}
