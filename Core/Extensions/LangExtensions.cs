﻿using System;
using System.Globalization;

namespace Core.Extensions
{
    public static class LangExtensions
    {
        public static string tr(this string value)
        {
            var culture = CultureInfo.CurrentCulture.Name;

            if (culture == "en-GB")
            {
                return value switch
                {
                    "home" => "Home",
                    _ => throw new Exception("not found")
                };
            }
            else if (culture == "en-US")
            {
                return value switch
                {
                    "home" => "Home",
                    _ => throw new Exception("not found")
                };
            }
            else
            {
                return value switch
                {
                    "home" => "anasayfa",
                    _ => throw new Exception("bulunamadı")
                };
            }

        }

    }
}
