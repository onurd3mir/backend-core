﻿using Castle.DynamicProxy;
using Core.CrossCuttingConcerns.Caching.Redis;
using Core.Utilities.Interceptors;
using Core.Utilities.IoC;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Aspect.Autofac.Caching
{
    public class RedisCacheRemoveAspect:MethodInterception
    {
        private string _pattern;
        private IRedisCacheManager _redisCacheManager;

        public RedisCacheRemoveAspect(string pattern)
        {
            _pattern = pattern;
            _redisCacheManager = ServiceTool.ServiceProvider.GetService<RedisCacheManager>();
        }

        protected override void OnSucces(IInvocation invocation)
        {
            _redisCacheManager.RemoveByPattern(_pattern);
        }
    }
}
