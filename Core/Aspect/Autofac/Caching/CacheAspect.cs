﻿using Castle.DynamicProxy;
using Core.CrossCuttingConcerns.Caching;
using Core.CrossCuttingConcerns.Caching.Redis;
using Core.Utilities.Interceptors;
using Core.Utilities.IoC;
using Core.Utilities.Results;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Aspect.Autofac.Caching
{
    public class CacheAspect: MethodInterception
    {
        private int _duration;
        private ICacheManager _cacheManager;
       
    
        public CacheAspect(int duration = 60)
        {
            _duration = duration;
            _cacheManager = ServiceTool.ServiceProvider.GetService<ICacheManager>();
            //_cacheManager = ServiceTool.ServiceProvider.GetService<RedisCacheManager>();
        }

   
        public override void Intercept(IInvocation invocation)
        {

            var returnType = invocation.Method.ReturnType.ReflectedType;

            var methodName = string.Format($"{invocation.Method.ReflectedType.FullName}.{invocation.Method.Name}");
            var argumants = invocation.Arguments.ToList();
            var key = $"{methodName}({string.Join(",", argumants.Select(x => x?.ToString() ?? "<Null>"))})";

            if (_cacheManager.IsAdd(key))
            {
                var data = _cacheManager.Get(key);
                invocation.ReturnValue = data;
                return;
            }

            invocation.Proceed();
            _cacheManager.Add(key, invocation.ReturnValue, _duration);
        }

    
    }
}
