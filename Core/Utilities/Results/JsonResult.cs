﻿using Newtonsoft.Json;
using System.Text;

namespace Core.Utilities.Results
{
    public class JsonResult
    {
        protected virtual byte[] Serialize(object item)
        {
            var jsonString = JsonConvert.SerializeObject(item);
            return Encoding.UTF8.GetBytes(jsonString);
        }

        protected virtual T Deserialize<T>(string[] serializedObject)
        {
            if (serializedObject == null)
                return default(T);

            string jsonString = "[";
            foreach (var item in serializedObject)
                jsonString += item + ",";
            jsonString += "]";
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        protected virtual string SerializeString(object item)
        {
            var jsonString = JsonConvert.SerializeObject(item);
            return jsonString;
        }

        protected virtual object DeserializeString(string serializedObject)
        {
            return JsonConvert.DeserializeObject(serializedObject);
        }

        protected virtual T DeserializeString<T>(string serializedObject)
        {
            return JsonConvert.DeserializeObject<T>(serializedObject);
        }

    }
}
