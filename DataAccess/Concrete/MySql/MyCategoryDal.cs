﻿using Core.DataAccess;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.MySql.Contexts;
using Entities.Concrete;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Concrete.MySql
{
    public class MyCategoryDal : EfEntityRepositoryBase<Category, MySqlNorthwindContext>, ICategoryDal
    {
        public List<Category> GetCategories()
        {
            var manager = new DapperManager(ConnectionStrings.mySqlConnection);

            string commadSql = @"SELECT CategoryId,CategoryName FROM categories";

            var result = manager.GetQuery<Category>(commadSql).ToList();

            return result;
        }
    }
}
