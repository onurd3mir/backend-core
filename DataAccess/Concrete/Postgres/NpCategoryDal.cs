﻿using Core.DataAccess;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using Entities.Concrete;
using DataAccess.Concrete.Postgres.Contexts;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Concrete.Postgres
{
    public class NpCategoryDal : EfEntityRepositoryBase<Category, PostgreNorthwindContext>, ICategoryDal
    {
        public List<Category> GetCategories()
        {
            var manager = new DapperManager(ConnectionStrings.postgreSqlConnection);

            string commadSql = @"SELECT categoryid,categoryname FROM public.categories";

            var result = manager.GetQuery<Category>(commadSql).ToList();

            return result;
        }
    }
}
