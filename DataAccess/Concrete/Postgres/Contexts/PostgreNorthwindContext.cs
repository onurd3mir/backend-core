﻿using Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Concrete.Postgres.Contexts
{
    public class PostgreNorthwindContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(ConnectionStrings.PostgreSql);
        }

        public DbSet<Product> products { get; set; }
        public DbSet<Category> categories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
           .Property(b => b.CategoryId).HasColumnName("categoryid");

            modelBuilder.Entity<Category>()
           .Property(b => b.CategoryName).HasColumnName("categoryname");
        }

    }
}
