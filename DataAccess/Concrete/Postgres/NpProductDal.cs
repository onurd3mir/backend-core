﻿using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.Postgres.Contexts;
using Entities.Concrete;
using Entities.Dtos;
using Entities.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Concrete.Postgres
{
    public class NpProductDal : EfEntityRepositoryBase<Product, PostgreNorthwindContext>, IProductDal
    {
        public List<Product> GetQuery(int first = 1, int last = 20, ProductFilter productFilter = null)
        {
            throw new NotImplementedException();
        }

        public List<TenMostExpensiveProduct> GetTenMostExpensiveProducts()
        {
            throw new NotImplementedException();
        }

        public List<TopSellingProduct> TopSellingProducts()
        {
            throw new NotImplementedException();
        }
    }
}
