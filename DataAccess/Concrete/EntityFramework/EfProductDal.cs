﻿using Core.DataAccess;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using Entities.Dtos;
using Entities.Filter;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;


namespace DataAccess.Concrete.EntityFramework
{
    public class EfProductDal : EfEntityRepositoryBase<Product, NorthwindContext>, IProductDal
    {
        public List<Product> GetQuery(int first = 1, int last = 20, ProductFilter productFilter = null)
        {
            using (var context = new NorthwindContext())
            {
                var result = context.Products.Where(p => p.ProductId > 0);

                if (productFilter.CategoryId != 0)
                {
                    result = result.Where(q => q.CategoryId == productFilter.CategoryId);
                }

                if (!string.IsNullOrEmpty(productFilter.ProductName))
                {
                    result = result.Where(q => q.ProductName.Contains(productFilter.ProductName));
                }

                if (productFilter.ProductId != 0)
                {
                    result = result.Where(q => q.ProductId == productFilter.ProductId);
                }

                first = (first - 1) * last;

                return result.Skip(first).Take(last).AsNoTracking().ToList();
            }
        }

        public List<TenMostExpensiveProduct> GetTenMostExpensiveProducts()
        {
            using (var context = new NorthwindContext())
            {
                return context.TenMostExpensiveProducts
                    .FromSqlRaw("[dbo].[Ten Most Expensive Products]")
                    .AsNoTracking().ToList();
            }
        }

        public List<TopSellingProduct> TopSellingProducts()
        {
            var manager = new DapperManager(ConnectionStrings.msSqlConnection);

            string commadSql = @"select p.ProductID,p.ProductName,ISNULL(o.count,0) as Count from products p (nolock) 
                                left join(
	                                select ProductID, COUNT(*) as count from [Order Details] (nolock) group by ProductID
                                ) o on p.ProductID=o.ProductID
                                order by o.count desc
                                ";

            var result = manager.GetQuery<TopSellingProduct>(commadSql).ToList();

            return result;
        }
    }
}
