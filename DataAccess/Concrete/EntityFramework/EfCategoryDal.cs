﻿using Core.DataAccess;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;


namespace DataAccess.Concrete.EntityFramework
{
    public class EfCategoryDal : EfEntityRepositoryBase<Category, NorthwindContext>, ICategoryDal
    { 
        public List<Category> GetCategories()
        {
            var manager = new DapperManager(ConnectionStrings.msSqlConnection);

            string commadSql = @"SELECT CategoryId,CategoryName FROM Categories";

            var result = manager.GetQuery<Category>(commadSql).ToList();

            return result;
        }
    }
}
