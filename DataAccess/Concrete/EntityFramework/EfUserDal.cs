﻿using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entities.Concrete;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfUserDal : EfEntityRepositoryBase<User, NorthwindContext>, IUserDal
    {
        public List<OperationClaim> GetCliams(User user)
        {
            using (var context = new NorthwindContext())
            {
                var result = from operationCliam in context.OperationClaims
                             join userOperationCliam in context.UserOperationClaims
                             on operationCliam.Id equals userOperationCliam.OperationClaimId
                             where userOperationCliam.UserId == user.Id
                             select new OperationClaim
                             { Id = operationCliam.Id, Name = operationCliam.Name };

                return result.ToList();
            }
        }
    }
}
