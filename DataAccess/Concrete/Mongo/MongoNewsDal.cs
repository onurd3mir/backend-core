﻿using Core.DataAccess.Mongo;
using DataAccess.Abstract;
using Entities.Mongo;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Concrete.Mongo
{
    public class MongoNewsDal:MongoEntityRepositoryBase<News>,INewsDal
    {
    }
}
