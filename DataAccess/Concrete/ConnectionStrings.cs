﻿using Microsoft.Data.SqlClient;
using MySql.Data.MySqlClient;
using Npgsql;

namespace DataAccess.Concrete
{
    public class ConnectionStrings
    {
        public static string MsSql => @"Server=192.168.133.130;Database=Northwind;User ID=sa;Password=1234Zxcv;";
        public static string MySql => @"server=localhost;database=northwind;user=root;password=1234";
        public static string PostgreSql => @"Host=localhost;Database=Northwind;Username=postgres;Password=1234";

        public static SqlConnection msSqlConnection => new SqlConnection(MsSql);
        public static MySqlConnection mySqlConnection => new MySqlConnection(MySql);
        public static NpgsqlConnection postgreSqlConnection => new NpgsqlConnection(PostgreSql);
    }
}
