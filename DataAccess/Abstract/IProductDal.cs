﻿using Core.DataAccess;
using Entities.Concrete;
using Entities.Dtos;
using Entities.Filter;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface IProductDal : IEntityRepository<Product>
    {
        List<TenMostExpensiveProduct> GetTenMostExpensiveProducts();
        List<Product> GetQuery(int first = 1, int last = 20, ProductFilter productFilter = null);
        List<TopSellingProduct> TopSellingProducts();
    }
}
