﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constants;
using Business.ValidationRules.FluentValidation;
using Core.Aspect.Autofac.Caching;
using Core.Aspect.Autofac.Logging;
using Core.Aspect.Autofac.Performance;
using Core.Aspect.Autofac.Transaction;
using Core.Aspect.Autofac.Validation;
using Core.CrossCuttingConcerns.Caching;
using Core.CrossCuttingConcerns.Caching.Redis;
using Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using Core.CrossCuttingConcerns.Validation;
using Core.Utilities.Business;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Entities.Dtos;
using Entities.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class ProductManager : IProductService
    {
        private IProductDal _productDal;
        private ICategoryService _categoryService;
        private IRedisCacheManager _redisCacheManager;

        public ProductManager(IProductDal productDal, ICategoryService categoryService,IRedisCacheManager redisCacheManager)
        {
            _productDal = productDal;
            _categoryService = categoryService;
            _redisCacheManager = redisCacheManager;
        }

        [ValidationAspect(typeof(ProductValidator), Priority = 2)]
        [MemoryCacheRemoveAspect("IProductService.Get", Priority = 4)]
        [RedisCacheRemoveAspect("IProductService.Get", Priority = 5)]
        [LogAspect(typeof(DatabaseLogger), Priority = 6)]
        //[SecuredOperation("Admin", Priority = 1)]
        [TransactionScopeAspect(Priority = 3)]
        public IResult Add(Product product)
        {
            #region validator
            //ProductValidator validator = new ProductValidator();
            //var result = validator.Validate(product);
            //if (!result.IsValid)
            //{
            //    throw new ValidationException(result.Errors);
            //}

            //IResult validate = ValidationTool.IsValid(new ProductValidator(), product);
            //if (!validate.Success) return new ErrorResult(validate.Message);
            #endregion

            IResult result = BusinessRules.Run(
                CheckIfProductNameExists(product.ProductName), 
                CheckIfCategoryExists(product.CategoryId)
            );

            if (!result.Success)
            {
                return result;
            }

            _productDal.Add(product);
            return new SuccessResult(Messages.ProductAdded);
        }

        private IResult CheckIfCategoryExists(int categoryId)
        {
            var result = _categoryService.GetById(categoryId);
            if (result.Data == null)
            {
                return new ErrorResult(Messages.CategoryExistsAny);
            }
            return new SuccessResult();
        }

        private IResult CheckIfProductNameExists(string productName)
        {
            var result = _productDal.Get(q => q.ProductName == productName);
            if (result != null)
            {
                return new ErrorResult(Messages.ProductNameExists);
            }
            return new SuccessResult();
        }

        [RedisCacheRemoveAspect("IProductService.Get", Priority = 2)]
        [MemoryCacheRemoveAspect("IProductService.Get", Priority = 1)]
        public IResult Delete(Product product)
        {
            _productDal.Delete(product);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public IDataResult<Product> GetById(int productId)
        {
            return new SuccessDataResult<Product>(_productDal.Get(q => q.ProductId == productId));
        }

        //[SecuredOperation("Admin")]
        //[PerformanceAspect(1)]
        public IDataResult<List<Product>> GetList()
        { 
            string key = "IProductService.GetList";
            if(_redisCacheManager.IsAdd(key))
            {
                return new SuccessDataResult<List<Product>>(_redisCacheManager.Get<List<Product>>(key));                
            }
            else
            {
                var data = _productDal.GetList().ToList();
                _redisCacheManager.Add(key, data, 3);
                return new SuccessDataResult<List<Product>>(data);
            }
        }

        //[SecuredOperation("Product.List,Admin", Priority = 1)]
        //[LogAspect(typeof(DatabaseLogger), Priority = 2)]
        //[CacheAspect(10, Priority = 3)]
        public IDataResult<List<Product>> GetListByCategory(int categoryId)
        {
            return new SuccessDataResult<List<Product>>(_productDal.GetList(q => q.CategoryId == categoryId).ToList());
        }

        public IDataResult<List<TenMostExpensiveProduct>> GetTenMostExpensiveProducts()
        {
            return new SuccessDataResult<List<TenMostExpensiveProduct>>
                (_productDal.GetTenMostExpensiveProducts().ToList());
        }

        [SecuredOperation("Admin")]
        [LogAspect(typeof(DatabaseLogger))]
        [TransactionScopeAspect]
        public IResult TransactionalOperations(Product product)
        {
            _productDal.Update(product);
            _productDal.Add(product);
            return new SuccessResult(Messages.ProductUpdated);
        }

        //[ValidationAspect(typeof(ProductValidator), Priority = 1)]
        [MemoryCacheRemoveAspect("IProductService.Get", Priority = 2)]
        [RedisCacheRemoveAspect("IProductService.Get", Priority = 3)]
        public IResult Update(Product product)
        {
            _productDal.Update(product);
            return new SuccessResult(Messages.ProductUpdated);
        }

        [PerformanceAspect(1)]
        public IDataResult<List<TopSellingProduct>> TopSellingProducts()
        {
            return new SuccessDataResult<List<TopSellingProduct>>(_productDal.TopSellingProducts());
        }
    }
}
