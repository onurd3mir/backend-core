﻿using Business.Abstract;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Mongo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Concrete
{
    public class NewsManager : INewsService
    {
        private readonly INewsDal _newsDal;

        public NewsManager(INewsDal newsDal)
        {
            _newsDal = newsDal;
        }

        public IDataResult<News> Add(News news)
        {
            return new SuccessDataResult<News>(_newsDal.Add(news));
        }

        public IDataResult<News> Get(string id)
        {
            return new SuccessDataResult<News>(_newsDal.Get(n => n.Id == id));
        }

        public IDataResult<List<News>> GetList()
        {
            return new SuccessDataResult<List<News>>(_newsDal.GetList());
        }

        public IResult Update(string id, News news)
        {
            _newsDal.Update(id, news);
            return new SuccessResult();
        }

        public IResult Delete(string id)
        {
            _newsDal.Delete(id);
            return new SuccessResult();
        }
    }
}
