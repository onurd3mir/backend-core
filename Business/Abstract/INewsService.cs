﻿using Core.Utilities.Results;
using Entities.Mongo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstract
{
    public interface INewsService
    {
        IDataResult<News> Get(string id);

        IDataResult<List<News>> GetList();

        IDataResult<News> Add(News news);

        IResult Update(string id,News news);

        IResult Delete(string id);

    }
}
