﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstract
{
    public interface IMyAggregateService
    {
        IProductService ProductService { get; }
        ICategoryService CategoryService { get; }
        IAuthService AuthService { get; }
        IUserService UserService { get; } 
    }
}
