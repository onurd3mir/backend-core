﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Constants
{
    public static class Messages
    {
        public static string ProductAdded = "Ürün Başarıyla Eklendi.";
        public static string ProductDeleted = "Ürün Başarıyla Silindi.";
        public static string ProductUpdated = "Ürün Başarıyla Güncellendi.";
        public static string CategoryAdded = "Kategori Başarıyla Eklendi.";
        public static string CategoryDeleted = "Kategori Başarıyla Silindi.";
        public static string CategoryUpdated = "Kategori Başarıyla Güncellendi.";
        public static string UserNotFound = "Kullanıcı Bulunamadı";
        public static string PasswordError = "Parola Yanlış";
        public static string SuccessfulLogin = "Giriş Başarılı";
        public static string UserAllReadyExists = "Kullanıcı Mevcut";
        public static string UserRegistered = "Kullanıcı Başarıyla Kaydedildi.";
        public static string AccessTokenCreate = "Token Başarıyla Oluşturuldu";
        public static string ProductNameExists = "Ürün İsmi Mevcut";
        public static string CategoryExistsAny = "Kategori Mevcut Değil";

    }
}
