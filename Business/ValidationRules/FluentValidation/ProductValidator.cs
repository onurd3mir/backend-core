﻿using Entities.Concrete;
using FluentValidation;

namespace Business.ValidationRules.FluentValidation
{
    public class ProductValidator:AbstractValidator<Product>
    {
        
        public ProductValidator()
        {
            RuleFor(p => p.ProductName).NotEmpty().WithMessage("Ürün İsmi Boş olamaz");
            //magic string kutulmak için contants dosyasına class olusturup yapabilirsin.
            //WithMessage kullanmaz isen kendi default mesajlarını verir.
            RuleFor(p => p.ProductName).Length(3,30);
            RuleFor(p => p.UnitPrice).NotEmpty().WithMessage("Fiyat Boş Olamaz");
            RuleFor(p => p.UnitPrice).GreaterThanOrEqualTo(1).WithMessage("O olamaaz");
            RuleFor(p => p.UnitPrice).GreaterThanOrEqualTo(10).When(p=>p.CategoryId==1);
            RuleFor(p => p.ProductName).Must(StartWithA).WithMessage("Ürün İsmi A ile Başlamalıdır.");//kendi özel kısıtlamamızı yazıyoruz must ile

        }

        private bool StartWithA(string arg)
        {
            if(!string.IsNullOrEmpty(arg))
                return arg.StartsWith("A");
            return false;
        }
    }
}
