﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Filter
{
    public class ProductFilter
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string ProductName { get; set; }
    }
}
