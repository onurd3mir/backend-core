﻿using Core.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Mongo
{
    public class News : DocumentDbEntity
    {
        public News()
        {
            this.DateAdded = DateTime.Now;
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime DateAdded { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CategoryId { get; set; }
    }
}
